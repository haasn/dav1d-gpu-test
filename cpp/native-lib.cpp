#include <android/log.h>
#include <jni.h>
#include <string>
#include <pthread.h>
#include <unistd.h>

#define LOG_TAG "dav1d"
#define PIPE_READ  0
#define PIPE_WRITE 1

int pipefd[2];
pthread_t loggingThread;

static void *logThreadFunction(void *arg) {
    ssize_t readSize;
    char buffer[1024];

    while ((readSize = read(pipefd[PIPE_READ], buffer, sizeof(buffer) - 1)) > 0) {
        // Null-terminate the string and log it
        buffer[readSize] = '\0';
        __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "%s", buffer);
    }

    return NULL;
}

extern "C" JNIEXPORT jboolean JNICALL
Java_com_example_ctest_MainActivity_initLogging(JNIEnv *env, jobject thiz) {
    // Create the pipe
    if (pipe(pipefd) == -1) {
        __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, "Error creating pipe: %s", strerror(errno));
        return false;
    }

    // Redirect stdout and stderr
    dup2(pipefd[PIPE_WRITE], STDOUT_FILENO);
    dup2(pipefd[PIPE_WRITE], STDERR_FILENO);

    // Create the logging thread
    if (pthread_create(&loggingThread, NULL, logThreadFunction, NULL) != 0) {
        __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, "Error creating thread: %s", strerror(errno));
        return false;
    }

    return true;
}

int main(int argc, char *const *argv);
static const char *const argv[] = {
        "dav1d",
        "--demuxer", "ivf",
        "-i", "-",
        "--muxer", "null",
        "-o", "-",
        "--realtime",
};

extern "C" JNIEXPORT jobject JNICALL
Java_com_example_ctest_MainActivity_decodeFile(JNIEnv *env, jobject thiz, int fd) {
    dup2(fd, STDIN_FILENO);
    main(sizeof(argv) / sizeof(argv[0]), (char *const *) argv);
    return NULL;
}