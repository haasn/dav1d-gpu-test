package com.example.ctest

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.DocumentsContract
import androidx.appcompat.app.AppCompatActivity
import com.example.ctest.databinding.ActivityMainBinding
import java.io.FileDescriptor
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.InputStream
import kotlin.system.exitProcess


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var path: Uri? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!initLogging())
            exitProcess(1)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.chooseFile.setOnClickListener {
            val intent = Intent()
                .setType("*/*")
                .addCategory(Intent.CATEGORY_OPENABLE)
                .putExtra(DocumentsContract.EXTRA_INITIAL_URI, "/sdcard/av1")
                .setAction(Intent.ACTION_OPEN_DOCUMENT)

            startActivityForResult(Intent.createChooser(intent, "Select a file"), 111)
        }

        binding.decode.setOnClickListener {
            path?.let {
                val fd = contentResolver.openFileDescriptor(it, "r")
                if (fd != null) {
                    Thread {
                        decodeFile(fd.detachFd())
                        fd.close()
                    }.start()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 111 && resultCode == RESULT_OK) {
            path = data?.data // The URI with the location of the file
            binding.fileName.text = path?.lastPathSegment;
        }
    }

    /**
     * A native method that is implemented by the 'ctest' native library,
     * which is packaged with this application.
     */
    external fun initLogging(): Boolean
    external fun decodeFile(fd: Int): Void

    companion object {
        // Used to load the 'ctest' library on application startup.
        init {
            System.loadLibrary("ctest")
        }
    }
}